#!/usr/bin/env bash
FILES=(
  './rp-db/docker-compose.yaml'
  './rp-admin-back/docker-compose.yaml'
  './rp-group/docker-compose.yaml'
  './rp-auth/docker-compose.yaml'
  './rp-pay/docker-compose.yaml'
  './rp-product/docker-compose.yaml'
  #'./rp-client-front/docker-compose.yaml'
  #'./rp-admin-front/docker-compose.yaml'
)

FILESTRING=$(echo ${FILES[@]} | sed 's/ / -f /g')

echo 'Init basic service and networks'
docker network create restaurant_purchasing
docker-compose -p restaurant_purchasing -f ./docker-compose.yaml $@

for i in "${FILES[@]}"
do
  docker-compose -p restaurant_purchasing -f $i $@ &
done
wait

sleep 3
echo 'Init global frontend load balancer'
docker-compose -p restaurant_purchasing -f ./docker-compose-app-nginx.yaml $@

echo 'DONE'
