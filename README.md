## Installation

After cloning this repository (by default it should be checked out at `restaurant-purchasing`), you need to checkout its submodules as well

	git submodule init # only needed for the first time, to register submodules
    git submodule update --recursive # checkout submodules
	git submodule foreach git checkout master # checkout master in each submodule
