#!/usr/bin/env bash
FILES=(
  './rp-db/'
  './rp-admin-back/'
  './rp-group/'
  './rp-auth/'
  './rp-pay/'
  './rp-group/'
  './rp-product/'
  #'./rp-admin-front/'
  #'./rp-client-front/'
)
FILESTRING=$(echo ${FILES[@]} | sed 's/ / \& /g')
FILESTRING+=' & wait'
CURRENT_FOLDER=$(pwd)

echo "Start running initialization scripts for following modules: "
printf '%s\n' "${FILES[@]}"

for folder in ${FILES[@]}; do
  cd ${CURRENT_FOLDER}/${folder}
  source init.sh &
done

wait
echo "DONE"
